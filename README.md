# MaterialSliders

Sliders are used to view and select a value(or range) from a set of data along a bar. They can use icons on both ends to represent numeric or relative scale. The range of values or the nature of the values can be communicated with icons.

This slider library provides default design to the user with an option for customization. Other than slider features from API version 9, it provides options to show current value, minimum value, maximum value of slider as well as option to set default value to the slider. It has some mandatory attributes. Trying to access library without mandatory attributes will push error in console, though these errors cannot terminate compilation.


## Installation

```ets 
npm i @ohos/MaterialSlider
```

## Usage Instruction

To be able to use sliders, below import statement should be used

```ets
import { MaterialSlider, SliderModel, SliderType } from '@ohos/MaterialSlider'
```

Access slider attributes through a object of SliderModel and customize the slider(if needed) using setter functions as
shown and finally pass the object to MaterialSlider.
<br>

## Continuous Slider

```ets
import { MaterialSlider, SliderModel, SliderType }  from '@ohos/MaterialSlider'
```

##### 1. Outset

```ets
//Creating object 
private sliderModel: SliderModel = new SliderModel(SliderType.Continue)
```

```ets
//Customization 
aboutToAppear(){
    this.sliderModel.setSliderStyle(SliderStyle.OutSet)
    this.sliderModel.setMin(100)
    this.sliderModel.setMax(1000)
    this.sliderModel.setStep(1)
    this.sliderModel.setCurrentValue(500)
    this.sliderModel.setShowSteps(false)
    this.sliderModel.setShowTips(true)
    this.sliderModel.setTrackThickness(8)
    this.sliderModel.setReverse(false)
    this.sliderModel.setDirection(Axis.Horizontal)
    this.sliderModel.setShowValue(true)
    this.sliderModel.setShowMin(false)
    this.sliderModel.setShowMax(false)
    this.sliderModel.setBlockColor("#ff0477ff")
    this.sliderModel.setTrackColor("#D0D0D0")
    this.sliderModel.setSelectedColor("#ff0477ff")
}    
```

```ets
//Passing parameters to MaterialSlider
MaterialSlider({
    obj : this.sliderModel,
    onCheckChange: this.onCheckChange
})
```

![outset](https://gitee.com/applibgroup/MaterialSliders/blob/master/screenshot/outsetSlider.png)

##### 2. Inset

```ets
//Creating object 
private sliderModel: SliderModel = new SliderModel(SliderType.Continue)
```

```ets
//Customization 
aboutToAppear(){
    this.sliderModel.setSliderSliderStyle(SliderStyle.InSet)
    this.sliderModel.setMin(0)
    this.sliderModel.setMax(100)
    this.sliderModel.setStep(1)
    this.sliderModel.setCurrentValue(40)
    this.sliderModel.setShowSteps(false)
    this.sliderModel.setShowTips(true)
    this.sliderModel.setTrackThickness(15)
    this.sliderModel.setReverse(false)
    this.sliderModel.setDirection(Axis.Horizontal)
    this.sliderModel.setShowValue(true)
    this.sliderModel.setShowMin(false)
    this.sliderModel.setShowMax(false)
    this.sliderModel.setBlockColor(Color.White)
    this.sliderModel.setTrackColor("#D0D0D0")
    this.sliderModel.setSelectedColor("#ff0477ff")
}    
```

```ets
//Passing parameters to MaterialSlider
MaterialSlider({
    obj : this.sliderModel,
    onCheckChange: this.onCheckChange
})
```
![inset](https://gitee.com/applibgroup/MaterialSliders/blob/master/screenshot/insetSlider.png)

## Discrete Slider


```ets
import { MaterialSlider, SliderModel, SliderType }  from '@ohos/MaterialSlider'
```

##### 1. Outset

```ets
//Creating object 
private sliderModel: SliderModel = new SliderModel(SliderType.Discrete)
```

```ets
//Customization 
aboutToAppear(){
    this.sliderModel.setSliderStyle(SliderStyle.OutSet)
    this.sliderModel.setMin(1000)
    this.sliderModel.setMax(10000)
    this.sliderModel.setStep(1000)
    this.sliderModel.setCurrentValue(3000)
    this.sliderModel.setShowSteps(true)
    this.sliderModel.setShowTips(true)
    this.sliderModel.setTrackThickness(8)
    this.sliderModel.setReverse(false)
    this.sliderModel.setDirection(Axis.Horizontal)
    this.sliderModel.setShowValue(true)
    this.sliderModel.setShowMin(false)
    this.sliderModel.setShowMax(false)
    this.sliderModel.setBlockColor("#ff0477ff")
    this.sliderModel.setTrackColor("#D0D0D0")
    this.sliderModel.setSelectedColor("#ff0477ff")
}    
```

```ets
//Passing parameters to MaterialSlider
MaterialSlider({
    obj : this.sliderModel,
    onCheckChange: this.onCheckChange
})            
```

![outset_dis](https://gitee.com/applibgroup/MaterialSliders/blob/master/screenshot/outsetSlider_dis.png)

##### 2. Inset

```ets
//Creating object 
private sliderModel: SliderModel = new SliderModel(SliderType.Discrete)
```

```ets
//Customization 
aboutToAppear(){
    this.sliderModel.setSliderStyle(SliderStyle.InSet)
    this.sliderModel.setMin(0)
    this.sliderModel.setMax(100)
    this.sliderModel.setStep(10)
    this.sliderModel.setCurrentValue(50)
    this.sliderModel.setShowSteps(true)
    this.sliderModel.setShowTips(true)
    this.sliderModel.setTrackThickness(15)
    this.sliderModel.setReverse(false)
    this.sliderModel.setDirection(Axis.Horizontal)
    this.sliderModel.setShowValue(true)
    this.sliderModel.setShowMin(true)
    this.sliderModel.setShowMax(true)
    this.sliderModel.setBlockColor(Color.White)
    this.sliderModel.setTrackColor("#D0D0D0")
    this.sliderModel.setSelectedColor("#ff0477ff")
}    
```

```ets
//Passing parameters to MaterialSlider
MaterialSlider({
    obj : this.sliderModel,
    onCheckChange: this.onCheckChange
})            
```

![inset_dis](https://gitee.com/applibgroup/MaterialSliders/blob/master/screenshot/insetSlider_dis.png)

## Compatibility

Supports OpenHarmony API version 9

## Code Contribution
If you find any problems during usage, you can submit an [Issue](https://gitee.com/applibgroup/MaterialSliders/issues) to us.
Of course, we also welcome you to send us [PR](https://gitee.com/applibgroup/MaterialSliders/pulls).

## Open Source License
This project is based on [Apache License 2.0](https://gitee.com/applibgroup/MaterialSliders/blob/master/LICENSE), please enjoy and
participate in open source freely.

### Reference:

Designed by : Bibek Lakra
